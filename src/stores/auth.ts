import type { User } from "@/entities";
import axios from "axios";
import { defineStore } from "pinia";
import { ref } from "vue";


export const useAuthStore = defineStore('auth', ()=> {
    const user = ref<User>();
    const stored = localStorage.getItem('user');
    if(stored) {
        user.value = JSON.parse(stored);
    }
    
    async function login(credentials:{email:string,password:string}) {
        const response = await axios.get('/api/account', {
            auth: {username: credentials.email, password:credentials.password}
        });
        localStorage.setItem('user',JSON.stringify(response.data));
        user.value = response.data;
    }
    async function logout() {
        await axios.get('/logout');
        localStorage.removeItem('user');
        user.value = undefined;
    }

    return {user,login,logout};


})