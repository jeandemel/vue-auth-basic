import axios from "axios";


axios.defaults.baseURL = 'http://localhost:8080'
axios.defaults.withCredentials = true;
axios.defaults.headers.common['X-Requested-With'] ='XMLHttpRequest';
